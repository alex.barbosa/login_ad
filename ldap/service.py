from ldap3 import Connection, Server
from ldap3.core.exceptions import LDAPException


class LdapService:
    """! Class for authentication on AD server."""

    @staticmethod
    def authenticate(method, credentials: dict) -> bool:
        """Try to authenticate on AD server."""
        try:
            if str(method) == 1:
                server = Server('BRCWBVS30.sa.bm.net', use_ssl=True, allowed_referral_hosts=0)
                conn = Connection(
                    server=server,
                    user=credentials["username"],
                    password=credentials["password"],
                    auto_bind='NONE'
                )
            else:
                server = Server('BRCWBVS30.sa.bm.net', use_ssl=True, allowed_referral_hosts=0)
                conn = Connection(
                    server,
                    'DC=sa,DC=bm,DC=net',
                    credentials["username"],
                    credentials["password"],
                )
            # conn.bind()
            print(server.info)
            conn.unbind()
            return True
        except Exception as error:
            raise error
